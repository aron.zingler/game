This puzzle was supposed to be easy, but somebody decided it should be a bit harder.
Can you find out who committed this crime? 


Hint: Focus your attention on first lines of any output.
More hints may be given on request.